#!/usr/bin/env bash
# SPDX-FileCopyrightText: Copyright © 2024 BlueBuild developers <bluebuild@xyny.anonaddy.com>
# SPDX-FileCopyrightText: Copyright © 2024-2025 Andrey Brusnik <dev@shdwchn.io>
#
# SPDX-License-Identifier: Apache-2.0

set -Eeuo pipefail

echo 'This is an example shell script'
echo 'Scripts here will run during build if specified in recipe.yml'

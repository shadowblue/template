# shadowblue template &nbsp; [![pipeline status](https://gitlab.com/shadowblue/template/badges/main/pipeline.svg)](https://gitlab.com/shadowblue/template/-/commits/main) &nbsp; [![Quay Images](https://img.shields.io/badge/Quay-Images-green)](https://quay.io/shadowblue/template)

## Quick setup your repo based on this template

1. Fork this repo
2. Edit [.gitlab-ci.yml](./.gitlab-ci.yml) and [template.yml](./recipes/template.yml)
3. Remove `cosign.pub`
4. Generate new sigstore keypair: `skopeo generate-sigstore-key --output-prefix cosign` (you can use empty password)
5. Add private key to GitLab CI:
   1. Go to Settings → CI/CD → Secure Files
   2. Click 'Upload File' and select your `cosign.private`
6. Commit new `cosign.pub` to your repo
7. Create project in your registry (Quay, Docker Hub, etc) and setup it in GitLab CI:
   * Set CD_REGISTRY (e.g. `quay.io`) and CD_NAMESPACE (e.g. `shadowblue`) variables in GitLab CI:
      1. Go to Settings → CI/CD → Variables (You can do that in Project or Group settings)
      2. Click 'Add variable'
      3. Enable `Protect variable`
      4. Fill key and value
   * Set CD_USERNAME and CD_PASSWORD variables in GitLab CI (use yours or bot's login/password):
      1. Go to Settings → CI/CD → Variables
      2. Click 'Add variable'
      3. Enable `Protect variable`
      4. Set Visibility to **Masked**
      5. Fill key and value
   * If you entered eny password for sigstore key, you should create COSIGN_PASSWORD masked variable and fill it with password
8. Update this README to describe your custom image
9. Push changes
10. Enable scheduled pipelines in GitLab CI:
   * Build → Pipeline schedules → Create a new pipeline schedule
   * I recommend to build every day after upstream images are ready (see schedule in specific image repo)

## Installation

> [!WARNING]
> [This is an experimental feature](https://www.fedoraproject.org/wiki/Changes/OstreeNativeContainerStable), try at your own discretion.

To rebase an existing atomic Fedora installation to the latest build (see [available tags](#available-tags) for other options):

- **Recommended:** reset all modifications of immutable image (layered packages, overrides, etc):
  ```
  rpm-ostree reset
  ```
- Rebase to the unsigned image, to get the proper signing keys and policies installed:
  ```
  rpm-ostree rebase ostree-unverified-registry:quay.io/shadowblue/template:latest
  ```
- Reboot to complete the rebase:
  ```
  systemctl reboot
  ```
- Note that on the first boot after rebase, the system will remove all flatpak applications installed from the Fedora repo and install applications from Flathub. After installation, there is a chance that the icons of these applications will be missing. Don't panic, after the next rebase+reboot this should be fixed. If not, try switching the dark/light theme. If nothing helps, open the issue in this repo.
- Then rebase to the signed image, like so:
  ```
  rpm-ostree rebase ostree-image-signed:docker://quay.io/shadowblue/template:latest
  ```
- Reboot again to complete the installation
  ```
  systemctl reboot
  ```

## Schedule

These images start building every Monday at 04:00 UTC. Usually the images are ready at 04:10 UTC.

## Supported versions

Supported:

- Rawhide
- Branched on a best effort scenario. I can forget to enable it when branched starts in Fedora and enable it a few days or weeks later
- Latest Fedora release until official EoL
- Previous Fedora release until official EoL

### Available tags

- :rawhide
- :latest (Tracks newest fedora version released. Version is replaced two weeks after the new release available.)
- :oldlatest (:latest version - 1)
- :${fedora_version} + 1 (e.g. :42. Available only if there is a branched version.)
- :${fedora_version} (e.g. :41)
- :${fedora_version} - 1 (e.g. :40)

Each tag has it's timestamp version for easier rollback and release pinning. E.g. :rawhide → :rawhide-20241030, :latest → :latest-20241030, etc.

### Tags expiration policy

All tags in registry for oldlatest version expire after 180 days. Tags for latest version expire after 365 days. Tags for rawhide and branched expire after 30 days.

## ISO

If build on Fedora Atomic, you can generate an offline ISO with the instructions available [here](https://blue-build.org/learn/universal-blue/#fresh-install-from-an-iso). These ISOs cannot unfortunately be distributed on GitLab for free due to large sizes, so for public projects something else has to be used for hosting.

## Verification

These images are signed with [Sigstore](https://www.sigstore.dev/)'s [cosign](https://github.com/sigstore/cosign). You can verify the signature by downloading the `cosign.pub` file from this repo and running the following command:

```bash
cosign verify --key cosign.pub quay.io/shadowblue/template:latest
```

## Contact us

- [Matrix Space](https://matrix.to/#/#shadowblue:matrix.org) (`#shadowblue:matrix.org`)
- [Telegram Chat](https://t.me/shadowblue_linux) (`@shadowblue_linux`)

## Credits

Forked from [BlueBuild's template](https://github.com/blue-build/template).

## License

```
Copyright 2024 BlueBuild developers <bluebuild@xyny.anonaddy.com>
Copyright 2024-2025 Andrey Brusnik <dev@shdwchn.io>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
